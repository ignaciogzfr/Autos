/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;
import java.sql.*;
/**
 *
 * @author Nigger
 */
public class Query extends Conexion{
    public String id ="";


// Login usuario
        public boolean login(String nick, String pass){
        PreparedStatement pst = null;
        ResultSet rs = null;

        try{
            String query = "Select * from usuario where Nick=? and Pass=?";
            pst = getConnection().prepareStatement(query);
            pst.setString(1, nick);
            pst.setString(2, pass);
            rs = pst.executeQuery();
            if(rs.absolute(1)){
            id = rs.getString(1);
            return true;
        }
        }    
        catch(SQLException e){
            System.out.println(e);
         return false;
        }
        finally{
            
                
            
            try{
                if (getConnection()!=null){
                    getConnection().close();
                if(pst!=null) pst.close();
                if (rs!=null) rs.close();
                }
            }
            catch(Exception e){
                System.out.println(e);
            }
        }
            return false;

        }
// Fin Login usuario



// Registrar usuario
        public boolean registro(String nick, String pass){
        PreparedStatement pst = null;
        ResultSet rs = null;

        try{
            String query = "INSERT INTO usuario(nick,pass) values(?,?)";
            pst = getConnection().prepareStatement(query);
            pst.setString(1, nick);
            pst.setString(2, pass);
            if(pst.executeUpdate()==1){
            return true;
        }
        }    
        catch(SQLException e){
            System.out.println(e);
            return false;
        } 
        finally{
            
                
            
            try{
                if (getConnection()!=null){
                    getConnection().close();
                if(pst!=null) pst.close();
                if (rs!=null) rs.close();
                }
            }
            catch(Exception e){
                System.out.println(e);
            }
        }
            return false;
        }
// Fin Registro
    

// Fin Consultas relacionadas al Usuario


// Consultas relacionadas al Auto

// Agregar



      public boolean publicar(String modelo,int marca,  int precio ){
       PreparedStatement pst = null;
       ResultSet rs = null;
       
       try{
         String query = "INSERT into auto(Modelo,idMarca,Precio) values(?,?,?)";  
           pst = getConnection().prepareStatement(query);
           pst.setString(1,modelo);
           pst.setInt(2,marca);
           pst.setInt(3,precio);
           
        
             if(pst.executeUpdate()==1){
            return true;
        }
       }catch(Exception e){
           System.err.println("error"+e);
           return false;
       }
      
          
      return false;
      }



// Fin Agregar 




        
// Eliminar 
    public boolean eliminar(int idAuto){
       PreparedStatement pst = null;
       
       try{
         String query = "delete from auto where idAuto = ? ";  
           pst = getConnection().prepareStatement(query);
           pst.setInt(1,idAuto);
             if(pst.executeUpdate()==1){
            return true;
        }
       }catch(Exception e){
           System.err.println("error"+e);
           return false;
       }
      
          
      return false;
      }
// Fin Eliminar 







// Update
     public boolean update(String Modelo,int idMarca,int precio, int idAuto){
       PreparedStatement pst = null;
       
       try{
         String query = "UPDATE Auto SET  Modelo=?,idMarca=?, Precio=? where idAuto = ?";  

           pst = getConnection().prepareStatement(query);
           pst.setString(1,Modelo);
           pst.setInt(2,idMarca);
           pst.setInt(3,precio);
           pst.setInt(4,idAuto);

             if(pst.executeUpdate()==1){
            return true;
        }
       }catch(Exception e){
           System.err.println("error "+e);
           return false;
       }
      
          
      return false;
      }
// Fin Update

// Fin Consultas relacionadas a Auto
}
