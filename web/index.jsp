<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Model.Query"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    </head>
    <body>  
    <div class="container" style="padding-top: 100px;">
        <div class="mx-auto mb-5">

<!-- Barra de Navegacion -->
                <nav class="navbar bg-light rounded">
<%
    if (session.getAttribute("autenticado")==null){
        out.println("<a href='login.jsp'  style='border:  3px solid white;' class='btn btn-outline-dark btn-lg mb-2   rounded-0'>Ingresa</a>");
        out.println("<a href='registro.jsp' style='border:  3px solid white;' class='btn btn-outline-dark btn-lg mb-2   rounded-0'>Registrate</a>");
    }else{

  out.println("<div class='form-inline'><button  style='border:  3px solid white;' class='btn btn-outline-primary btn-lg mb-2' data-toggle='modal' data-target='#modalAgregar'>Publicar Auto</button>");
  out.println("<a href='logout.jsp'  style='border:  3px solid white;' class='btn btn-outline-danger btn-lg mb-2 '>Cerrar Sesion</a></div");
    }
    
%>
                </nav>
                </div>
                

<!-- Tabla de Contenidos -->
                <table  class="table table-info mt-3 tabla-autos rounded">
                      <thead>
                        <tr>
                          <th scope="col">Marca</th>
                          <th scope="col">Modelo</th>
                          <th scope="col">Precio</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                        <tbody>
                            <%
                            PreparedStatement pst;
                            ResultSet rs;
                            Query q = new Query();
                            String query = "Select marca, modelo, precio, idAuto, Auto.idMarca from Auto"
                            +" INNER JOIN Marca on Auto.idMarca = Marca.idMarca";

                            pst = q.getConnection().prepareStatement(query);
                            rs = pst.executeQuery();
                            while (rs.next()){
                            out.println("<tr><td name='marca'>"+rs.getString(1)+"</td>");
                            out.println("<td name='modelo'>"+rs.getString(2)+"</td>");
                            out.println("<td name='precio'>"+"$"+rs.getString(3)+"</td>");
                             out.println("<td id='Acciones' class='text-center'><div class='form-inline'><form method='GET' action='eliminar' class = 'form-delete'><input type='hidden' name='id' value='"+rs.getString(4)+"'><button type='submit'class='btn btn-danger btn-delete'><i class='fas fa-trash-alt'></i></button></form>");
                             out.println("<button data-toggle='modal' data-target='#modalEdit' class='btn btn-info btn-editar' idMarca= '"+rs.getString(5)+"'idAuto='"+rs.getString(4)+"' marca='"+rs.getString(1)+"' modelo='"+rs.getString(2)+"' precio='"+rs.getString(3)+"'><i class='fas fa-edit'></i></button></div></td>");
                                }
                             %>
                            
                        </tbody>


















<!-- Modal Agregar -->
<div class="modal fade" id="modalAgregar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nuevo Aviso</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                    <form method="GET" action="ingresar">
                  <div class="modal-body">
                    
                          <div class="form-group">
                            <input type="text" name="modelo" class="form-control mb-3" id="marcaModal" placeholder="modelo">
                            <select name="marca" class="custom-select custom-select-light mb-3">
                             <option selected disabled>Marca</option>
                         <%   
                    pst= q.getConnection().prepareStatement("Select * from marca");
                    rs = pst.executeQuery();
                        while(rs.next()){
                            out.println("<option id='marca'"+rs.getString(1)+" value='"+rs.getString(1)+"'>"+rs.getString(2)+"</option>");
                        }
                    %>

                              </select>
                              <input placeholder="Precio" name="precio" class="form-control"></input>
                          </div> 
                                
                        
                </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary rounded-0" data-dismiss="modal">Cerrar</button>
                        <button class="btn  btn-primary rounded-0" type="submit">Guardar</button>
                    </div>
                </form>                      
                </div>
                </div>
            </div>
<!-- Fin Modal -->



















<!-- Modal Editar -->
<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nuevo Aviso</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                    <form method="GET" action="editar">
                  <div class="modal-body">
                    
                          <div class="form-group">
                            <input type="text" name="modelo" class="form-control mb-3" id="marcaModal" placeholder="modelo">
                            <select name="marca" class="custom-select custom-select-light mb-3">
                             <option selected disabled>Marca</option>
                         <%   
                    pst= q.getConnection().prepareStatement("Select * from marca");
                    rs = pst.executeQuery();
                        while(rs.next()){
                            out.println("<option id='marca'"+rs.getString(1)+" value='"+rs.getString(1)+"'>"+rs.getString(2)+"</option>");
                        }
                    %>

                              </select>
                              <input placeholder="Precio" name="precio" class="form-control"></input>
                          </div> 
                                
                        
                </div>
                    <div class="modal-footer">
                        <input type="hidden" name="idAuto">
                        <button type="button" class="btn btn-secondary rounded-0" data-dismiss="modal">Cerrar</button>
                        <button class="btn  btn-primary rounded-0" type="submit">Guardar</button>
                    </div>
                </form>                      
                </div>
                </div>
            </div>
<!-- Fin Modal -->
            </div>
    </body>
























<!-- Scripts BootStrap -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2"></script>
    <script>
       
    $(".btn-delete").on("click",function(event){
        event.preventDefault();
        
        swal({
            title: 'Estas seguro?',
            text: "No podras revertir este paso!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Aceptar',
            cancelButtonText: "Cancelar"
       }).then((result) => {
         if (result.value) {
           $(this).closest(".form-delete").submit();
         }
       });
       
    })
    
        
    </script>
<script>
$(document).ready( function () {

      $(".btn-editar").on("click",function(){

        var id = $(this).attr("idAuto");
        var marca =  $(this).attr("marca");
        var modelo = $(this).attr("modelo");
        var precio = $(this).attr("precio");
        $("#modalEdit input[name='modelo']").val(modelo);
        $("#modalEdit select[name='marca']").val(marca);        
        $("#modalEdit input[name='precio']").val(precio);
        $("#modalEdit input[name='idAuto']").val(id);

        
      })

      $('.tabla-autos').DataTable({
          language: {
          processing:     "Procesando...",
          search:         "Busqueda",
          lengthMenu:     "Listar de _MENU_resultados",
          info:           "Listando de _START_ de _END_ de un total de  _TOTAL_ resultados",
          infoEmpty:      "Listando 0 resultados",
          infoFiltered:   "(Filtro de _MAX_ resultados totales)",
          infoPostFix:    "",
          loadingRecords: "Procesando",
          zeroRecords:    "No se encontró ningún resultado",
          emptyTable:     "Tabla vacía",
          paginate: {
              first:      "Primero",
              previous:   "Anterior",
              next:       "Siguiente",
              last:       "Último"
          },
          aria: {
              sortAscending:  ": Ordenar Ascendentemente",
              sortDescending: ": Ordenar Descendentemente"
          }
        }
      });
  } )
  </script>

</html>
